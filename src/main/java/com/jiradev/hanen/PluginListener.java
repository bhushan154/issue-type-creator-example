package com.jiradev.hanen;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.ProjectSchemeAssociationManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.context.ProjectContext;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.List;


/**
 * Created by bhushan154 on 31/03/17.
 */


public class PluginListener implements InitializingBean, DisposableBean {


    private final IssueTypeManager issueTypeManager;
    private final EventPublisher eventPublisher;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final ProjectManager projectManager;
    private final FieldManager fieldManager;

    public PluginListener(EventPublisher eventPublisher,
                          IssueTypeManager issueTypeManager,
                          IssueTypeSchemeManager issueTypeSchemeManager,
                          ProjectManager projectManager,
                          FieldManager fieldManager){
        this.issueTypeManager = issueTypeManager;
        this.eventPublisher = eventPublisher;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.projectManager = projectManager;
        this.fieldManager = fieldManager;

    }

    @Override
    @PreDestroy
    public void destroy() throws Exception {
    //Handle plugin disabling or un-installation here
        System.out.print("----------------------------");
        System.out.print("Plugin Uninstalled");
        System.out.print("----------------------------");
    }


    @Override
    @PostConstruct
    public void afterPropertiesSet() throws Exception {
        System.out.print("----------------------------");
        System.out.print("Plugin Installed");
        //Handle plugin enabling or installation here

        //Create a new Issue Type
        issueTypeManager.createIssueType("Idea", "This is a new issue type,", new Long(10000));

        //Create a new Issue Type Scheme
        FieldConfigScheme newIssueTypeScheme = issueTypeSchemeManager.create("Hanens Scheme", "This is a demo of a new Issue Type Scheme", Arrays.asList("Idea"));

        //Get the project you want to update the issue type scheme for
        Project project = projectManager.getProjectObjByKeyIgnoreCase("test");

        if(project != null){
            Long[] projectIds = new Long[0];
            projectIds[0] = project.getId();
            List<JiraContextNode> contexts = CustomFieldUtils.buildJiraIssueContexts
                    (false, projectIds, projectManager);
            //Update the Issue Type Scheme
            ComponentAccessor.getFieldConfigSchemeManager().updateFieldConfigScheme(newIssueTypeScheme, contexts, fieldManager.getConfigurableField(IssueFieldConstants.ISSUE_TYPE));
        }
        System.out.print("----------------------------");
    }

}
